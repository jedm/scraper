<?php
//include('simple_html_dom.php');
error_reporting(E_ALL);
class Amazon {
/*
	Title
	Price
	Number in stock
*/			
	public function run($url) {

		$html = file_get_html($url);
		
		//This gets the title 
		foreach($html->find('h1') as $title) {	
			$data['title'] = $title->plaintext;	
		}

		//This gets the price and stock info
		foreach($html->find('span[class]') as $span) {
				foreach($span->find('a[class=olpBlueLink]') as $a) {
					$link = $a->innertext;
					$ex=explode("&nbsp;",$link);
                			$tmp[] = $ex[0];
                			$data['quantity']=array_sum($tmp);	
				
					foreach($span->find('.price') as $price) {
						$tmp_p[]=$price->plaintext;
					}
				}
		}
				
		foreach($html->find('b[class=priceLarge]') as $test) {
			$tmp_p[]=$test->plaintext;
		}
		
		arsort($tmp_p);
                $data['price']=$tmp_p[0];
			
		if(!isset($data['quantity'])) {
			
			foreach($html->find('span[class=availGreen]') as $amount) {
                       		$quantity = $amount->plaintext;
                       		if(strpos($quantity,"In Stock.") !== false) {
                       		        $data['quantity'] = "In stock.";
                       		} else {
					$data['quantity'] = "No data";
				}
               		}
		}
	
			
	
	$html->clear();
	

	return $data;

	}
    
}
	



?>
