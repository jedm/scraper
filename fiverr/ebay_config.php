<?php
include('simple_html_dom.php');
error_reporting(E_ALL);
class Ebay {
/*
	Title
	Price
	Number in stock
*/			
	public function run($url) {
//$url  = "http://www.ebay.com/itm/301496705695";
		$html = file_get_html($url);
		//This gets the title 
		foreach($html->find('h1') as $title) {	
			$dirty_title = explode("  ",$title->plaintext);
			
			$i = max($dirty_title);
			if(strpos($i,'Details about') !== false) {
                        	$f_title = str_replace("&nbsp;","",$dirty_title[1]);
				$data['title'] = trim($f_title);
                	} 
			else {
				$data['title'] = $i;
			}
		}

		//This gets the price and stock info
		foreach($html->find('span[id=prcIsum]') as $span) {
					$price = $span->plaintext;
					$ex=explode(" ",$price);
					$data['price']=$ex[1];
		}
		
		foreach($html->find('div[class=msgPad]') as $msg) {
			$error['type'] = "Out";
			$error['message'] = preg_replace("/\s\s+/","",$msg->plaintext);
			$error['url'] = $url;
			$error['flag'] = "R";
			$data['quantity'] = "None";
		}
		foreach($html->find('span[id=qtySubTxt]') as $qty_raw) {
			$error['type'] = "Low";
			$error['message'] = preg_replace("/\s\s+/","",$qty_raw->plaintext);
			$error['url'] = $url;
			$error['flag'] = "Y";
			$data['quantity'] = "Low";
		}
	 
		if(strlen($error['message'])<3) {
		$data['status'] = "Normal";
		$data['quantity'] = "In stock";
		} else {

		$data['status']=$error;
		}
	$html->clear();
	

	return $data;

	}
    
}
	



?>
